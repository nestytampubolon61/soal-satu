package com.investree.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "transaksi")
public class Transaksi implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_peminjam")
    User users_peminjam;

    @ManyToOne
    @JoinColumn(name = "id_meminjam")
    User users_meminjam;

    @Column(name = "tenor", nullable = false, length = 11)
    private int tenor;

    @Column(name = "total_pinjaman", nullable = false, length = 11)
    private Double total_pinjaman;

    @Column(name = "bunga_persen", nullable = false, length = 11)
    private Double bunga_persen;

    @Column(name = "status", nullable = false, length = 11)
    private String status;


}
