package com.investree.demo.model;

import lombok.Data;
import org.w3c.dom.Text;


import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_detail")
public class UserDetail implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User users;

    @Column(name = "nama", nullable = false, length = 11)
    private String nama;

    @Column(name = "alamat", nullable = false, length = 11)
    private Text alamat;

}
