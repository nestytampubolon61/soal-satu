package com.investree.demo.repository;

import com.investree.demo.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    @Query("select c from User c")
    public List<User> getList();

    @Query("select c from User c WHERE c.id = :id")
    public User getbyID(@Param("id") Long id);

}
